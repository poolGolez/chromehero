<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>CRUD Operations</title>

        <!-- Fonts -->
        <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <br><br>


        <form method="post" action="{{ url('crud') }}">
            <div class="form-group row">
                {{ csrf_field() }}
              <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Title</label>
              <div class="col-sm-10">
                <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="title" name="title">
              </div>
            </div>
            <div class="form-group row">
              <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Post</label>
              <div class="col-sm-10">
                <textarea name="post" rows="8" cols="80"></textarea>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2"></div>
              <input type="submit" class="btn btn-primary">
            </div>
          </form>

    </body>
</html>
